import { Box, Grid, Container, Card, CardContent, Typography, CardActions, Button } from '@mui/material'
import {useState, useEffect} from 'react';
import { useHover } from "@uidotdev/usehooks";
import PropTypes from 'prop-types';

function getRandomColor() {
	const colors = ["green", "blue", "purple", "red", "pink"];
	return colors[Math.floor(Math.random() * colors.length)];
  }

const List = () => {
	const [data, setData] = useState([])

	const [ref, hovering] = useHover();

	const link = process.env.REACT_APP_LINK
	List.propTypes = {
		link : PropTypes.string.isRequired
	}

	const backgroundColor = hovering
	  ? `var(--${getRandomColor()})`
	  : "var(--charcoal)";

	 const fetchUserData = () => {
    fetch(link)
      .then(response => {
        return response.json()
      })
      .then(data => {
        setData(data)
      })
  }

  useEffect(() => {
    fetchUserData()
  }, [])

	return (
		<Container >
			<Grid container sx={{ marginTop: '24px'}}>
				<Grid xs={10} md={7} >
					<Box sx={{ border: '1px solid red', height: '50vh', marginRight: '20px'}}>
						<Grid container>
							<Grid xs={6}>
							<Card sx={{ minWidth: 275, marginX: '20px', marginTop: '20px' }}>
							<CardContent>
								<Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
									Word of the Day
								</Typography>
								<Typography sx={{ mb: 1.5 }} color="text.secondary">
									adjective
								</Typography>
								<Typography variant="body2">
									well meaning and kindly.
									<br />
									{'"a benevolent smile"'}
								</Typography>
							</CardContent>
							<CardActions>
								<Button size="small" >Learn More</Button>
							</CardActions>
						</Card>
							</Grid>
							<Grid xs={6}>
							<Card sx={{ minWidth: 275, marginX: '20px', marginTop: '20px' }}>
							<CardContent>
								<Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
									Word of the Day
								</Typography>
								<Typography sx={{ mb: 1.5 }} color="text.secondary">
									adjective
								</Typography>
								<Typography variant="body2">
									well meaning and kindly.
									<br />
									{'"a benevolent smile"'}
								</Typography>
							</CardContent>
							<CardActions>
								<Button size="small" >Learn More</Button>
							</CardActions>
						</Card>
							</Grid>
						</Grid>
						
					</Box>
        </Grid>
        <Grid xs={12} md={4}>
					<Box sx={{ border: '1px solid', height: '50vh'}} color={process.env.REACT_APP_COLOR_PRIMARY.toString()}>
						<Box sx={{ padding: '20px' }}>
														<Box sx={{ paddingTop: '10px' }}>
										<Grid container>
											<Grid xs={3}>
												<img src={process.env.EACT_APP_IMAGE} alt="image" height={60} width={60} style={{ borderRadius: '100%'}} />
											</Grid>
											<Grid xs={8}>
												<Typography  ref={ref} color={{ backgroundColor }} fontWeight={500} fontSize={'20px'}>
												 {hovering ? "Pokemon" : `${data.name}` }
												</Typography>
											</Grid>
										</Grid>
									</Box>
						</Box>
					</Box>
        </Grid>
			</Grid>
		</Container>
	)
}

export default List