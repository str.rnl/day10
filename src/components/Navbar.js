import React from 'react'
import {Box, AppBar, Button, Typography, Toolbar} from '@mui/material'

const Navbar = ({ handleModal }) => {
	return (
		<Box sx={{ flexGrow: 1 }}>
			<AppBar position="static">
				<Toolbar>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						My App
					</Typography>
					<Button sx={{
						background: '#00FFFF',
						fontWeight: 600,
					}} variant='contained' color="inherit" onClick={handleModal}>Add User</Button>
				</Toolbar>
			</AppBar>
		</Box>
	)
}

export default Navbar