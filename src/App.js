import List from './pages/List'
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import './App.css';

function App() {
	
  return (
    <>
			<Navbar/>
			<List/>
			<Footer />
		</>
  );
}

export default App;